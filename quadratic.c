#include <stdio.h>
#include <math.h>

// Function to calculate the discriminant
double calculateDiscriminant(double a,double b, double c) {
    return b*b-4*a*c;
}
int main() {
   // Input coefficients
double a, b, c;
printf("Enter coefficients a, b, c: ");
scanf("%If %If %If", &a, &b, &c);

// Validate input
if (a==0) {
    printf("Coefficient 'a' cannot be zero. Exiting.\n");
    return 1; //Exit with an error code
}
// Calculate discriminant
double discriminant =
calculateDiscriminant(a,b,c);

// Check discriminant value
  if (discriminant > 0) {
    // Real and distinct roots
    double root1 =(-b+
    sqrt(discriminant)) / (2*a);
    double root2 =(-b-
    sqrt(discriminant))/(2*a);
          printf("Roots are real and distinct: %.2If and %.2If\n", root1,root2);
} else if (discriminant==0) {
// Real and equal roots
double root= -b/(2*a);
printf("Roots are real and equal: %.2If\n", root);
} else {
    // Complex roots
 printf("Roots are complex\n");
 }
 return 0; // Exit successfully
}
